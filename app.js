class Game {

    canvas = null
    context = null

    //default game settings
    gameSettings = {
        row: 10,
        col: 10,
        cellSize: 10,
        cellPadding: 0,

        //0 - use requestAnimationFrame
        //1 - use setInterval
        animationMode: 0,
        animationTimerValue: 10
    }

    //need for game
    grid = []
    computedAlive = []
    played = false
    generation = 0
    generationChangeEvent = null
    timeLagChangeEvent = null

    timeMark = 0

    constructor( elementId, gameSettings ) {

        for (let key in this.gameSettings) {
            if( gameSettings[key] ){
                this.gameSettings[key] = gameSettings[key]
            }
        }

        this.canvas = document.getElementById(elementId);

        let canvasStyles = window.getComputedStyle(this.canvas, null)
        let offsetX = parseInt(canvasStyles.getPropertyValue("padding-left"))
        let offsetY = parseInt(canvasStyles.getPropertyValue("padding-top"))

        this.canvas.width = this.gameSettings.row * this.gameSettings.cellSize + 1
        this.canvas.height = this.gameSettings.col * this.gameSettings.cellSize + 1

        if(this.canvas && this.canvas.getContext) {

            this.context = this.canvas.getContext('2d');

            this.drawArea()

            for (let x = 0; x < this.gameSettings.row; x++){
                this.grid[x] = []
                for (let y = 0; y < this.gameSettings.col; y++){
                    this.grid[x][y] = 0
                }
            }

            this.canvas.addEventListener('mousedown', (e) => {

                let originX = e.offsetX - offsetX
                let originY = e.offsetY - offsetY

                if( originX > this.canvas.width || originY > this.canvas.height ) return

                let cordX = Math.ceil( originX / this.gameSettings.cellSize ) - 1
                let cordY = Math.ceil( originY / this.gameSettings.cellSize ) - 1

                this.reviveCell( cordX, cordY )

            })

            if( this.gameSettings.animationMode == 0 ){
                window.requestAnimationFrame( this.draw.bind(this) );
            } else {
                setInterval(this.draw.bind(this), this.gameSettings.animationTimerValue)
            }

        } else {
            console.error('Error')
        }
    }

    timeMark1(){
        this.timeMark = new Date().getTime();
    }

    timeMark2(){
        if(this.timeLagChangeEvent) this.timeLagChangeEvent( (new Date().getTime() - this.timeMark) + 'ms' )
    }

    draw(){
        this.timeMark1()
        if( this.gameSettings.animationMode == 0 ){
            window.requestAnimationFrame( this.draw.bind(this) )
        }
        if( this.played ) {
            this.run()
        }
        this.timeMark2()
    }

    reviveCell(x,y){
        this.computedAlive = this.computedAlive.filter(e => !(e.x == x && e.y == y) )
        if( this.grid[x][y] == 0){
            this.computedAlive.push({
                x: x,
                y: y,
            })
            this.grid[x][y] = 1
            this.drawCell(x,y)
        } else {
            this.grid[x][y] = 0
            this.eraseCell(x,y)
        }
    }

    normCordX( x ){
        if( x < 0 ) return (this.gameSettings.row - 1) + x + 1
        if( x > this.gameSettings.row-1 ) return x - this.gameSettings.row
        return x
    }

    normCordY( y ){
        if( y < 0 ) return (this.gameSettings.col - 1) + y + 1
        if( y > this.gameSettings.col-1 ) return y - this.gameSettings.col
        return y
    }

    checkRules(x,y){

        let xLeft = this.normCordX( x-1 )
        let xRight = this.normCordX( x+1 )
        let yTop = this.normCordY( y-1 )
        let yBottom = this.normCordY( y+1 )

        let sum =
            this.grid[xLeft][yTop] +
            this.grid[x][yTop] +
            this.grid[xRight][yTop] +

            this.grid[xLeft][y] +
            this.grid[xRight][y] +

            this.grid[xLeft][yBottom] +
            this.grid[x][yBottom] +
            this.grid[xRight][yBottom]

        let result = false;

        if (this.grid[x][y] && sum < 2) {
            result = false;
        }
        if (this.grid[x][y] && (sum === 2 || sum === 3)) {
            result = true;
        }
        if (this.grid[x][y] && sum > 3) {
            result = false;
        }
        if (!this.grid[x][y] && sum === 3) {
            result = true;
        }
        return result
    }

    run(){
        if( !this.computedAlive.length ) return

        let newState = []

        this.computedAlive.forEach(cords => {

            for( let x = cords.x - 1; x <= cords.x + 1; x++){
                for( let y = cords.y - 1; y <= cords.y + 1; y++) {

                    let nX = this.normCordX( x )
                    let nY = this.normCordY( y )



                    let currentCellState = this.checkRules( nX, nY )

                    if( !newState.filter( e => (e.x == nX && e.y == nY) ).length ){
                        newState.push({
                            x: nX,
                            y: nY,
                            value: currentCellState
                        })
                    }
                    if ( currentCellState == this.grid[nX][nY] ) continue

                    if( currentCellState == 1 ){
                        this.drawCell(nX, nY)
                    } else {
                        this.eraseCell(nX, nY)
                    }
                }
            }

        })

        this.computedAlive = newState.filter(cords => cords.value == 1)

        newState.forEach( cords => {
            this.grid[cords.x][cords.y] = cords.value
        })

        this.generation++
        if(this.generationChangeEvent) this.generationChangeEvent(this.generation)
    }

    drawArea(){
        this.context.lineWidth = 1;
        this.context.strokeRect(0.5, 0.5, this.canvas.width - 1, this.canvas.height - 1);
        for (let x = 1; x < this.gameSettings.row; x++){
            this.context.beginPath();
            this.context.moveTo(x * this.gameSettings.cellSize + 0.5 , 0 );
            this.context.lineTo(x * this.gameSettings.cellSize + 0.5 , this.canvas.height - 1);
            this.context.stroke();
        }
        for (let y = 1; y <= this.gameSettings.col; y++){
            this.context.beginPath();
            this.context.moveTo(0 , y * this.gameSettings.cellSize+ 0.5);
            this.context.lineTo(this.canvas.width - 1 , y * this.gameSettings.cellSize + 0.5);
            this.context.stroke();
        }
    }

    drawCell(x, y){
        let xStart = x * this.gameSettings.cellSize + this.gameSettings.cellPadding/2+1
        let yStart = y * this.gameSettings.cellSize + this.gameSettings.cellPadding/2+1
        let width = this.gameSettings.cellSize  - this.gameSettings.cellPadding-1
        let height = this.gameSettings.cellSize - this.gameSettings.cellPadding-1
        this.context.fillRect( xStart, yStart, width, height )
    }

    eraseCell(x,y){
        let xStart = x * this.gameSettings.cellSize + this.gameSettings.cellPadding/2+1
        let yStart = y * this.gameSettings.cellSize + this.gameSettings.cellPadding/2+1
        let width = this.gameSettings.cellSize  - this.gameSettings.cellPadding-1
        let height = this.gameSettings.cellSize - this.gameSettings.cellPadding-1
        this.context.clearRect( xStart, yStart, width, height )
    }

    play(){
        this.played = true
    }

    pause(){
        this.played = false
    }

    plusOneStep(){
        this.run()
    }

    on(event, func){
        if( event === 'onGenerationChange' ){
            this.generationChangeEvent = func
        }
        if( event === 'onTimeLagChange' ){
            this.timeLagChangeEvent = func
        }
    }

    randomize(){
        for (let x = 0; x < this.gameSettings.row; x++){
            for (let y = 0; y < this.gameSettings.col; y++){
                let chance = Math.floor(Math.random() * 15 )
                if( chance == 2 ){
                    this.reviveCell(x,y)
                }
            }
        }
    }

    addFigure(figure){
        let x0 = this.normCordX( Math.floor(Math.random() * this.gameSettings.row ) )
        let y0 = this.normCordY( Math.floor(Math.random() * this.gameSettings.col ) )

        let rev = figure[0].map((_, colIndex) => figure.map(row => row[colIndex]));

        for (let x = 0; x < rev.length; x++){
            for (let y = 0; y < rev[x].length; y++){
                if( rev[x][y] )
                    this.reviveCell(this.normCordX( x0 + x ),this.normCordY( y0 + y ))
            }
        }

    }
}




window.onload = function(){

    window.game = new Game('game-life', {
        row: 350,
        col: 150,
        cellSize: 5,
        cellPadding: 0,
        animationMode: 0,
        animationTimerValue: 5
    })

    for (let i = 0; i <= 9; i++)
        game.addFigure([
            [0,0,1,],
            [1,0,1,],
            [0,1,1,],
        ])
    game.addFigure([
        [1,],
        [1,],
        [1,],
    ])
    game.addFigure([
        [1,1,1],
    ])

    for (let i = 0; i <= 6; i++)
        game.addFigure([
            [0,0,1,0,0,0],
            [1,0,0,0,1,0],
            [0,0,0,0,0,1],
            [1,0,0,0,0,1],
            [0,1,1,1,1,1]
        ])

    for (let i = 0; i <= 7; i++)
    game.addFigure([
        [0,1,0,],
        [1,0,1,],
        [1,0,1,],
        [0,1,0,],
    ])

    let timeLagEl = document.getElementById('timelag')
    game.on('onTimeLagChange', timeLag => {
        timeLagEl.innerText = timeLag
    })

    let generationEl = document.getElementById('generation')
    game.on('onGenerationChange',generation => {
        generationEl.innerText = generation
    })

    document.getElementById('play').addEventListener('click', (e) => {
        if(game.played){
            game.pause()
            e.target.textContent  = 'Play'
        } else {
            game.play()
            e.target.textContent  = 'Pause'
        }
    })

    document.getElementById('onestep').addEventListener('click', (e) => {
        game.plusOneStep()
    })

}
